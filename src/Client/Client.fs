module Client

open Elmish
open Elmish.React

open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.PowerPack.Fetch

open Shared

open Fulma
open Fulma.Layouts
open Fulma.Elements
open Fulma.Elements.Form
open Fulma.Components
open Fulma.BulmaClasses

open Fulma.BulmaClasses.Bulma
open Fulma.BulmaClasses.Bulma.Properties
open Fulma.Extra.FontAwesome


type Model = 
  { Comment : string
    Name    : string
    Score   : Score option 
    Loading : bool 
    Results : VotingResults option}

type Msg =
| SetComment of string
| SetName    of string
| SetScore   of Score
| Submit
| GotResults of Result<VotingResults, exn>


module Server = 

  open Shared
  open Fable.Remoting.Client
  
  /// A proxy you can use to talk to server directly
  let api : IVotingProtocol = 
    Proxy.remoting<IVotingProtocol> {
      use_route_builder Route.builder
    }
    

let init () : Model * Cmd<Msg> =
  let model = 
    { Comment = ""
      Name    = ""
      Score   = None 
      Loading = false
      Results = None}
  let cmd =
    Cmd.none
  model, cmd

let mkVote (model : Model) : Vote =
  { Comment = model.Comment
    Name    = model.Name
    Score   = defaultArg model.Score Good }

let update (msg : Msg) (model : Model) : Model * Cmd<Msg> =
  let model' =
    match msg with
    | SetComment comment -> { model with Comment = comment }
    | SetName    name    -> { model with Name    = name }
    | SetScore   score   -> { model with Score   = Some score }
    | Submit             -> { model with Loading = true }
    | GotResults (Ok r)  -> { model with Loading = false 
                                         Results = Some r }
    | GotResults _       -> { model with Loading = false }

  let cmd =
    match msg with
    | Submit -> 
      Cmd.ofAsync
        Server.api.addVote
        (mkVote model')
        (Ok >> GotResults)
        (Error >> GotResults)  
    | _ ->
      Cmd.none      
  model', cmd

let show = function
| Some x -> string x
| None -> "Loading..."

let navBrand =
  Navbar.Brand.div [ ] 
    [ Navbar.Item.a 
        [ Navbar.Item.Props [ Href "https://safe-stack.github.io/" ] ] 
        [ img [ Src "https://safe-stack.github.io/images/safe_top.png"
                Alt "Logo" ] ] 
      Navbar.burger [ ] 
        [ span [ ] [ ]
          span [ ] [ ]
          span [ ] [ ] ] ]

let navMenu =
  Navbar.menu [ ]
    [ Navbar.End.div [ ] 
        [ Navbar.Item.a [ ] 
            [ str "Home" ] 
          Navbar.Item.a [ ]
            [ str "Examples" ]
          Navbar.Item.a [ ]
            [ str "Documentation" ]
          Navbar.Item.div [ ]
            [ Button.a 
                [ Button.Color IsWhite
                  Button.IsOutlined
                  Button.Size IsSmall
                  Button.Props [ Href "https://github.com/SAFE-Stack/SAFE-template" ] ] 
                [ Icon.faIcon [ ] 
                    [ Fa.icon Fa.I.Github; Fa.fw ]
                  span [ ] [ str "View Source" ] ] ] ] ]

let field input =
  Field.div [ ]
    [ Field.body [ ]
        [input ] ]

let onChange action =
  OnChange (fun e -> action !!e.target?value)        

let scoreIcon = function
| Good -> Fa.I.SmileO
| SoSo -> Fa.I.MehO 
| Poor -> Fa.I.FrownO
let scoreColor (model : Model) (score : Score) = 
  match model.Score with
  | None -> IsWhite
  | Some s when s <> score -> IsWhite
  | _ -> 
    match score with  
    | Good -> IsSuccess
    | SoSo -> IsWarning 
    | Poor -> IsDanger 
let scores (model : Model) (dispatch : Msg -> unit) =
  let item score =
    Level.item [ ]
      [ Button.a 
          [ Button.Color (scoreColor model score) 
            Button.Disabled model.Loading
            Button.OnClick (fun _ -> dispatch (SetScore score)) ]
          [ Icon.faIcon [ ]
              [ Fa.icon (scoreIcon score)
                Fa.fa2x ] ]]
                
  Level.level [ Level.Level.IsMobile ]
    [ item Good 
      item SoSo 
      item Poor ]

let comment (model : Model) (dispatch : Msg -> unit) =
  Textarea.textarea 
    [ Textarea.Placeholder "Comment" 
      Textarea.DefaultValue model.Comment
      Textarea.Disabled model.Loading
      Textarea.Props [ onChange (SetComment >> dispatch)] ]
    [ ]
let name (model : Model) (dispatch : Msg -> unit) =
  Input.text 
    [ Input.Placeholder "Name" 
      Input.DefaultValue model.Name
      Input.Disabled model.Loading 
      Input.Props [ onChange (SetName >> dispatch)] ]
let submit (model : Model) (dispatch : Msg -> unit) =
  Button.a 
    [ Button.Color IsPrimary
      Button.IsFullwidth
      Button.OnClick (fun _ -> dispatch Submit)
      Button.IsLoading model.Loading ]
    [ str "Submit" ]

let formBox (model : Model) (dispatch : Msg -> unit) =
  Box.box' [ CustomClass "cta" ]
    [ field (scores  model dispatch) 
      field (comment  model dispatch) 
      field (name     model dispatch)   
      field (submit   model dispatch)  ] 
let resultsBox (model : VotingResults) (dispatch : Msg -> unit) =
  let item score =
    let count = defaultArg (Map.tryFind score model.Scores)
    Level.item [ ]
      [ div
          [ ]
          [ Icon.faIcon [ ]
              [ Fa.icon (scoreIcon score)
                Fa.fa2x ] 
            h2 [] [ str (string count)]]]

  Box.box' [ CustomClass "cta" ]
    [ Level.level [ Level.Level.IsMobile ]
        [ item Good 
          item SoSo 
          item Poor ] 
      Content.content [ Content.Size IsSmall ]
        [ h3 [] [ str "Comments" ] 
          ol [ Style [TextAlign "left"]]
            [ for (name, comment) in model.Comments ->
              li []
                [ i [] [str (sprintf "'%s'" comment)]
                  str (sprintf " - %s" name) ] ] ] ]

let containerBox (model : Model) (dispatch : Msg -> unit) =
  match model.Results with
  | None -> formBox model dispatch
  | Some r -> resultsBox r dispatch 

let card icon heading body =
  Column.column [ Column.Width (Column.All, Column.Is4) ]
    [ Card.card [ ]
        [ div
            [ ClassName (Card.Classes.Image + " " + Alignment.HasTextCentered) ]
            [ i [ ClassName ("fa fa-" + icon) ] [ ] ]
          Card.content [ ]
            [ Content.content [ ]
                [ h4 [ ] [ str heading ]
                  p [ ] [ str body ]
                  p [ ]
                    [ a [ Href "#" ]
                        [ str "Learn more" ] ] ] ] ] ]

let features = 
  Columns.columns [ Columns.CustomClass "features" ]
    [ card "paw" "Tristique senectus et netus et." "Purus semper eget duis at tellus at urna condimentum mattis. Non blandit massa enim nec. Integer enim neque volutpat ac tincidunt vitae semper quis. Accumsan tortor posuere ac ut consequat semper viverra nam."
      card "id-card-o" "Tempor orci dapibus ultrices in." "Ut venenatis tellus in metus vulputate. Amet consectetur adipiscing elit pellentesque. Sed arcu non odio euismod lacinia at quis risus. Faucibus turpis in eu mi bibendum neque egestas cmonsu songue. Phasellus vestibulum lorem sed risus."
      card "rocket" "Leo integer malesuada nunc vel risus." "Imperdiet dui accumsan sit amet nulla facilisi morbi. Fusce ut placerat orci nulla pellentesque dignissim enim. Libero id faucibus nisl tincidunt eget nullam. Commodo viverra maecenas accumsan lacus vel facilisis." ]

let intro =
  Column.column 
    [ Column.CustomClass "intro"
      Column.Width (Column.All, Column.Is8)
      Column.Offset (Column.All, Column.Is2) ]
    [ h2 [ ClassName "title" ] [ str "Perfect for developers or designers!" ]
      br [ ]
      p [ ClassName "subtitle"] [ str "Vel fringilla est ullamcorper eget nulla facilisi. Nulla facilisi nullam vehicula ipsum a. Neque egestas congue quisque egestas diam in arcu cursus." ] ]

let tile title subtitle content =
  article [ ClassName "tile is-child notification is-white" ]
    [ yield p [ ClassName "title" ] [ str title ]
      yield p [ ClassName "subtitle" ] [ str subtitle ]
      match content with
      | Some c -> yield c
      | None -> () ]

let content txts =
  Content.content [ ]
    [ for txt in txts -> p [ ] [ str txt ] ]

module Tiles =
  let hello = tile "Hello World" "What is up?" None

  let foo = tile "Foo" "Bar" None

  let third = 
    tile 
      "Third column"
      "With some content"
      (Some (content ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis."]))

  let verticalTop = tile "Vertical tiles" "Top box" None
  
  let verticalBottom = tile "Vertical tiles" "Bottom box" None

  let middle = 
    tile 
      "Middle box" 
      "With an image"
      (Some (figure [ ClassName "image is-4by3" ] [ img [ Src "http://bulma.io/images/placeholders/640x480.png"] ]))

  let wide =
    tile
      "Wide column"
      "Aligned with the right column"
      (Some (content ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis."]))

  let tall =
    tile
      "Tall column"
      "With even more content"
      (Some (content 
              ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam."
               "Suspendisse varius ligula in molestie lacinia. Maecenas varius eget ligula a sagittis. Pellentesque interdum, nisl nec interdum maximus, augue diam porttitor lorem, et sollicitudin felis neque sit amet erat. Maecenas imperdiet felis nisi, fringilla luctus felis hendrerit sit amet. Aenean vitae gravida diam, finibus dignissim turpis. Sed eget varius ligula, at volutpat tortor."
               "Integer sollicitudin, tortor a mattis commodo, velit urna rhoncus erat, vitae congue lectus dolor consequat libero. Donec leo ligula, maximus et pellentesque sed, gravida a metus. Cras ullamcorper a nunc ac porta. Aliquam ut aliquet lacus, quis faucibus libero. Quisque non semper leo."]))

  let side = 
    tile
      "Side column"
      "With some content"
      (Some (content ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis."]))

  let main =
    tile
      "Main column"
      "With some content"
      (Some (content ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis."]))


let sandbox =
  div [ ClassName "sandbox" ]
    [ Tile.ancestor [ ]
        [ Tile.parent [ ] 
            [ Tiles.hello ]
          Tile.parent [ ] 
            [ Tiles.foo ]
          Tile.parent [  ] 
            [ Tiles.third ] ]
      Tile.ancestor [ ]
        [ Tile.tile [ Tile.IsVertical; Tile.Size Tile.Is8 ]
            [ Tile.tile [ ] 
                [ Tile.parent [ Tile.IsVertical ] 
                    [ Tiles.verticalTop
                      Tiles.verticalBottom ]
                  Tile.parent [ ] 
                    [ Tiles.middle ] ]
              Tile.parent [ ]
                [ Tiles.wide ] ]
          Tile.parent [ ]
            [ Tiles.tall ] ]
      Tile.ancestor [ ]
        [ Tile.parent [ ]
            [ Tiles.side ]
          Tile.parent [ Tile.Size Tile.Is8 ]
            [ Tiles.main ] ] ]

let footerContainer =
  Container.container [ ]
    [ Content.content [ Content.CustomClass Alignment.HasTextCentered ] 
        [ p [ ] 
            [ str "futile attempt" ]
          p [ ]
            [ a [ Href "https://github.com/SAFE-Stack/SAFE-template" ]
                [ Icon.faIcon [ ] 
                    [ Fa.icon Fa.I.Github; Fa.fw ] ] ] ] ]

let imgSrc = "https://fsharp.org/img/logo/fsharp256.png"

let view (model : Model) (dispatch : Msg -> unit) =
  div [ ]
    [ Hero.hero 
        [ Hero.Color IsPrimary
          Hero.IsMedium
          Hero.IsBold ]
        [ Hero.head [ ]
            [ Navbar.navbar [ ]
                [ Container.container [ ] 
                    [ navBrand
                      navMenu ] ] ]
          Hero.body [ ]
            [ Container.container [ Container.CustomClass Alignment.HasTextCentered ]
                [ Level.level [ ]
                    [ Level.item [ ]
                        [ Image.image [ Image.Is128x128 ] 
                            [ img 
                                [ Src imgSrc 
                                  Style [ Border "2px solid" ] ] ] ] ]
                  h1 [ ClassName "title" ] 
                    [ str "SAFE Demo" ]
                  div [ ClassName "subtitle" ]
                    [ str "Score My Attempt at SAFE" ] ] ] ]
      
      containerBox model dispatch
      
      // Container.container [ ]
      //   [ features
      //     intro
      //     sandbox ]

      // footer [ ClassName "footer" ]
      //   [ footerContainer ] 
      ]

  
#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
|> Program.withHMR
#endif
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
